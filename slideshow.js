/**
 * Slideshow...
 * 
 * requires:
 * --include [/slideshow/slideshow.js] and [/slideshow/slideshow.css]
 * --parent ul, containing child li's.
 * ----default classes are [ul.sSlideshow] and [li.sSlide], but you can pass a new class in the [slideParentClass] and [slideChildClass] parameters.
 * --slide images [1000px * 300px] in /slideshow/img/ named [slide0.jpg, slide1.jpg, etc.]
 *
 */

SLIDE = {};
SLIDE.show = function() {

	return {

		init:function( arrParams ) { // pass parameters array...

			// set default parameters...
			window.params = {
				interval: 7,						// slide interval in seconds
				controlPosition: 'bottom',			// top, bottom, left, right, none
				pauseOnHover: true,					// if true, pause on mouseover
				firstSlide: 0,						// slide to start on (count from zero) TODO: or 'random'
				slideWrapperClass: 'sSlideshow',	// class of slideshow wrapper
				// NOTE: if you want to change the wrapper class, you have to manually update slideshow.css... maybe not the best.
				transitionDuration: 1.5				// duration of slide transition in seconds
				// TODO: set slideshow wrapper height/width parameters...
			};

			// apply passed parameters to params...
			for( param in arrParams )
				params[param] = arrParams[param];

			// check if slideshow parent ul exists...
			if ( $( '.' + params.slideWrapperClass + ' ul li' ).length === 0 ) {
				console.log( 'Slideshow requires a ul wrapped in a div."' + params.slideWrapperClass + '" to exist in your markup.' );
				return false; // slideshow parent ul does not exist. exit function.

			// slideshow parent ul exists. continue...
			} else {

				// slideshow parent ul is display:none in css, show it when function has been initialized. progressively enhance like a boss...
				// also, apply slideChildClass to slideshow child li's...
				$( '.' + params.slideWrapperClass ).show(); //.children().addClass(params.slideWrapperClass);

				// get all slideshow child li's as array...
				arrSlides = $( '.' + params.slideWrapperClass + ' ul li' ).toArray();
				countSlides = $( arrSlides ).size(); // TODO: number of slides in arrSlides

				// apply ordered id's to slideshow child li's...
				for ( slide in arrSlides )
					arrSlides[slide].setAttribute( 'id', 'slide' + slide );

				// set bg images...
				this.images();

				// make controls...
				this.controls();

				// change to first slide...
				// TODO: if params.firstSlide = 'random', start on random slide
				window.current = params.firstSlide;
				this.changeSlide( params.firstSlide );

				// all set, run it!
				this.autoSlide();
			}
		},

		controls:function() { // make controls...

			if ( params.controlPosition && params.controlPosition != 'none' ) {

				// initialize controls...
				var controls = document.createElement( 'ol' );

				for ( slide in arrSlides ) { // create a control child li for each slideshow child li...
					var control = document.createElement( 'li' );
					control.setAttribute( 'id', 'control' + slide );
					controls.appendChild( control );
				}

				// append controls parent ul to slideshow parent ul...
				$( '.' + params.slideWrapperClass ).append( controls );

				// add position class to controls parent ul...
				$( '.' + params.slideWrapperClass + ' ol' ).addClass( params.controlPosition );

				function centerControls() {
					if ( params.controlPosition == 'right' || params.controlPosition == 'left' ) {
						var topValue = ( ( $( '.' + params.slideWrapperClass ).height() ) - ( $( '.' + params.slideWrapperClass + ' ol' ).height() ) ) / 2;
						$( '.' + params.slideWrapperClass + ' ol' ).css( 'top', topValue + 'px' );
					} else if ( params.controlPosition == 'top' || params.controlPosition == 'bottom' ) {
						var leftValue = ( ( $( '.' + params.slideWrapperClass ).width() ) - ( $( '.' + params.slideWrapperClass + ' ol' ).width() ) ) / 2;
						$( '.' + params.slideWrapperClass + ' ol' ).css( 'left', leftValue + 'px' );
					}
				}

				// center controls initially...
				centerControls();

				// center controls on resize event...
				$( window ).resize( function() {
					centerControls();
				});

				// get all control child li's as array...
				window.arrControls = $( '.' + params.slideWrapperClass + ' ol li' ).toArray();

				// set click event...
				$( '.' + params.slideWrapperClass + ' ol li' ).click( function() { 
					SLIDE.show.changeSlide( $( this ).index() ) 
				});
			}
		},

		images:function() { // make array of images and add them as bg's of slides...

			window.arrImages = new Array();

			for ( slide in arrSlides ) {
				// push to arrImages...
				arrImages.push( '/slideshow/img/s' + slide + '.jpg' );

				// apply bg to slides...
				arrSlides[slide].style.backgroundImage = 'url(' + arrImages[slide] + ')';
			}

			// testing...
			alert( 'arrSlides = ' + window.arrSlides );
			alert( 'arrImages = ' + window.arrImages );

		},

		advance:function() {
			var nextSlide = ( window.current >= 0 && window.current < ( countSlides - 1 ) ) ? ( window.current + 1 ) : 0;
			return nextSlide;
		},

		changeSlide:function( changeTo ) {

			// window.current is 0-indexed, jquery selector is 1-indexed...
			var oldIndex = window.current;
			// if no changeTo is set, advance to the next slide...
			var newIndex = changeTo !== undefined ? changeTo : this.advance();
			var $oldSlide = $( '.' + params.slideWrapperClass + ' ul li:nth-child(' + ( oldIndex + 1 ) + ')' );
			var $newSlide = $( '.' + params.slideWrapperClass + ' ul li:nth-child(' + ( newIndex + 1 ) + ')' );

			// update window.current...
			window.current = newIndex;

			// remove any .current from all controls, then add .current to current control...
			$( '.' + params.slideWrapperClass + ' ol li' ).removeClass( 'current' );
			arrControls[window.current].className += 'current';

			// if not clicking on the current slide, change to the newSlide from the oldSlide...
			if ( oldIndex == newIndex ) {
				$newSlide.show( 0 );
			} else {
				$oldSlide.fadeOut( params.transitionDuration * 1000 );
				$newSlide.fadeIn( params.transitionDuration * 1000 );
			}
		},

		autoSlide:function( passedCurrent ) {

			var slideTimer = setInterval( function() { SLIDE.show.changeSlide( passedCurrent ) } , params.interval * 1000 );

			if ( params.pauseOnHover === true ) {
				
				$( '.' + params.slideWrapperClass ).mouseover( function() { // on mouseover, stop timer...
					clearInterval( slideTimer );
				}).mouseout( function() { // on mouseout, restart timer...
					slideTimer = setInterval( function() { SLIDE.show.changeSlide() }, params.interval * 1000 );
				});
			}
		}

	}
}();