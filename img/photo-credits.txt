The featured photos on the osh.uconn.edu homepage were submitted to the US Department of Labor flickr page by the following people:

slide0.jpg - Melanie Mesaros
slide1.jpg - Robert Rodriguez
slide2.jpg - Jay Bell
slide3.jpg - Pete Campbell
slide4.jpg - Angelo Garcia, Jr.
slide5.jpg - Steve Baranowski
slide6.jpg - Skip Pennington